'''
Created on 26 nov. 2019

# maya start up
path = r'D:\work\eclipse_workspace'  # <- path to where the tool folder is


# define env var for refernce path
import os
os.environ['EXAMPLE_PATH'] = os.path.join(path, 'cache_export', 'examples')

# open scene
from maya import cmds
ref_scene = os.path.join(path, 'cache_export', 'examples', 'refScene.ma')
cmds.file(ref_scene, o=1, f=1)



import sys
sys.path.append(path)


from cache_export import simple_dialog
reload(simple_dialog)  # <- to take changes that you made
ui = simple_dialog.main()  # need to store the dialog in the ui variable so it's not garbage collected

@author: eduardo
'''
import sys
import os
import logging
import collections

from PySide2 import QtWidgets

from maya import cmds, mel

import loadUiType

# add lucidity
libs_path = os.path.join(os.path.dirname(__file__), 'libs')
sys.path.append(libs_path)
import lucidity


# set a logger for the script!
logger = logging.getLogger('cache_export.simple_dialog')
handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

ABC_EXPORT_PLUGIN = 'AbcExport'


DEFAULT_SEL = 'GEO_FINAL_GRP'

RADIO_MAP = {'part_radioButton': 'part',
             'full_radioButton': 'full'}

# create base class from ui file
MAIN_UI_FILE_REL_PATH = 'ui\\cache_export.ui'
MAIN_UI_FULL_PATH = os.path.join(os.path.dirname(__file__), MAIN_UI_FILE_REL_PATH)
MAIN_FORM_CLASS, MAIN_BASE_CLASS = loadUiType.loadUiType(MAIN_UI_FULL_PATH)


class MainUI(MAIN_FORM_CLASS, MAIN_BASE_CLASS):
    ''' This class implements the main ui '''

    def __init__(self, template_sel=None, parent=None):

        # configure parent class and apply ui definition
        super(MainUI, self).__init__(parent=parent)
        self.setupUi(self)

        # start frame range
        start = cmds.playbackOptions(q=True, min=True)
        self.spinBox_start.setValue(start)

        # end frame range
        end = cmds.playbackOptions(q=True, max=True)
        self.spinBox_end.setValue(end)

        # wire up ui
        self.button_export.clicked.connect(self.onExportClicked)

        # if there is something on template_sel, use that, if not,
        # use default value stored on DEFAULT_SET
        self.default_selection = template_sel or DEFAULT_SEL

        # connect on refresh button
        self.refresh_pushButton.clicked.connect(self.populateAssets)
        self.refresh_pushButton.clicked.connect(self.populateList)
        # connect enter pressed on selection line edit
        self.sel_lineEdit.returnPressed.connect(self.populateList)

        # connect on selection changed on list
        self.sel_listWidget.itemSelectionChanged.connect(self.onPartialListSelectionChanged)
        self.listWidget.itemSelectionChanged.connect(self.onFullListSelectionChanged)

        self.setPathSampleDefault()

        # populate the namespace
        self.populateAssets()
        self.populateList()

        self.export_type = 'full'

        # fill with default templates
        self.setDefaultSceneTemplate()
        self.setDefaultOutputTemplate()

    @property  # <- important this converts this function in a property getter
    def strip_namespaces(self):
        ''' returns whether the strip namespace checkbox is checked '''
        return self.strip_ns_checkBox.isChecked()

    @property  # <- important this converts this function in a property getter
    def show_folder(self):
        ''' returns whether the show folder checkbox is checked '''
        return self.show_folder_checkBox.isChecked()

    @property  # <- important this converts this function in a property getter
    def export_type(self):
        ''' returns the export type ('part' or 'full') depending on the radio buttons'''

        for radio_button_name, response in RADIO_MAP.items():
            radio_button = getattr(self, radio_button_name)
            if radio_button.isChecked():
                return response

    @export_type.setter  # <- important this converts this function in a property setter
    def export_type(self, export_type):
        ''' sets the export type  with 'part' or 'full' by modifying the radio buttons'''

        for radio_button_name, response in RADIO_MAP.items():
            if response.lower() == export_type.lower():
                radio_button = getattr(self, radio_button_name)
                radio_button.setChecked(True)

    # partial namespace UI ---

    @property  # <- important this converts this function in a property getter
    def default_selection(self):
        ''' getter for the field'''
        # get the text from the line edit
        text = self.sel_lineEdit.text() or ''

        # split by commas in case there are many names
        return text.split(',')

    @default_selection.setter  # <- important this converts this function in a property setter
    def default_selection(self, value):
        ''' setter for the field'''
        self.sel_lineEdit.setText(value)

    def populateList(self):
        ''' populates the main list '''

        # clear list to avoid duplication on refresh
        self.sel_listWidget.clear()

        # find all elements per namespace that match the selection name
        item_list = self.findSelectionInScene()
        item_list.sort()  # sorting for display in order

        # add items to the list
        for item in item_list:

            # create item with a name and a parent widget (the list)
            list_item = QtWidgets.QListWidgetItem(item, parent=self.sel_listWidget)

            # save original name in a variable inside the item
            list_item.maya_object_name = item

            # add item to list and set it as selected
            self.sel_listWidget.addItem(list_item)
            list_item.setSelected(True)

    def findSelectionInScene(self):
        ''' find the maya scene transforms that matches the naming on every namespace'''

        # get all namespaces
        ns_list = self.getRefNamespaces()

        all_sel_list = []

        # loop thorugh all namespaces
        for ns in ns_list:

            # get the elements for this namespace
            sel_list = self.getDefaultSelectionForNs(ns)

            # add all together in the all_sel_list
            all_sel_list.extend(sel_list)

        return all_sel_list

    def getDefaultSelectionForNs(self, ref_ns):
        ''' get the element that match the select criteria for a namespace
        Args:
            ref_ns (str): the namespace name
        Returns:
            list[str]: the list of elements matching the criteria on namespace
        '''
        # read selectio tokens from ui
        search_list = self.default_selection

        # search for selection withing the namespace, one criteria at a time
        default_sel_list = []
        for search in search_list:

            # list transform matching this search criteria (get long name)
            found_list = cmds.ls(cmds.ls(search, r=1), type='transform', long=1)

            # filter to keep only the ones in this namespace
            found_for_ns = [e for e in found_list if self.getNameSpace(e) == ref_ns]

            # add them to the list of found for namespace
            for found in found_for_ns:
                if found not in default_sel_list:
                    default_sel_list.append(found)

        return default_sel_list

    def getRefNamespaces(self):
        ''' get all the namespace in current scene '''

        # list all ref nodes. Some won't be valid references. We will filter that later
        ref_node_list = cmds.ls(type='reference')

        ref_ns_list = []

        # go through all reference nodes
        for ref_node in ref_node_list:

            # lets try to get the namespace, if it fails, it's because its not a file reference
            try:
                ns = cmds.referenceQuery(ref_node, ns=1, shortName=1)
                ref_ns_list.append(ns)

            except Exception, e:  # @UnusedVariable
                pass  # beware, not always a good idea to ignore errors!

        return ref_ns_list

    def getNameSpace(self, element):
        ''' get the namespace of a maya element '''

        # clean name to keep only the short name
        name = element.split('|')[-1]

        # if no : on name, then it has no namespace
        if ':' not in name:
            return ''

        # if it does, we split the name and keep the first part
        ns = name.split(':')[0]
        return ns

    def onPartialListSelectionChanged(self):
        ''' calback executed when selection cahnges on partial list '''
        self.updatePathSample()
        self.showSelectionInScene()

        # refresh ui!
        # QtWidgets.QApplication.instance().processEvents()

    def showSelectionInScene(self):
        ''' selects in scene what is selected on the list '''

        # get selection from list
        selection_in_list = self.getListSelection()

        # select on maya scene
        cmds.select(selection_in_list, r=1)

    def getListSelection(self):
        ''' list elements selected on the list '''

        # get selected items
        selected_item_list = self.sel_listWidget.selectedItems()

        selection_list = []

        # loop through selected items
        for selected_item in selected_item_list:

            # get maya name form item (remember we stored it when creating the item)
            maya_object_name = selected_item.maya_object_name

            # add the maya name to the list
            selection_list.append(maya_object_name)

        return selection_list
    # full namespace UI ---

    def populateAssets(self):
        ''' add cameras in scene to combo box
        '''

        # list visible meshes
        mesh_list = cmds.ls(v=True, type='mesh')

        # convert to transforms
        transform_list = []
        for m in mesh_list:
            parent = cmds.listRelatives(m, p=True)[0]
            transform_list.append(parent)

        namespace_list = []
        for transform in transform_list:

            # no namespace no cache
            if ':' not in transform:
                continue

            ns = transform.split(':')[0]

            # dont duplicate on list
            if ns in namespace_list:
                continue

            namespace_list.append(ns)

        self.listWidget.clear()

        for ns in namespace_list:
            self.listWidget.addItem(ns)

    def getSelectedNamespaces(self):
        ''' Get selected names in the namespace list
        Args:
            None
        Returns:
            list[str]: the list of selected namespace
        '''

        selected_listItems = self.listWidget.selectedItems()

        # ns_list = [s.text() for s in selected_listItems]

        ns_list = []
        for s in selected_listItems:
            ns = s.text()
            ns_list.append(ns)

        return ns_list

    def onFullListSelectionChanged(self):
        ''' callback executed when selection changes on full list'''
        self.updatePathSample()

    # export part ---

    def onExportClicked(self):
        ''' callback executed when export button is clicked '''

        logger.info('Exporting')

        if self.export_type == 'full':
            return self.exportFullNamespaces()

        if self.export_type == 'part':
            return self.exportPartialNamespaces()

        cmds.confirmDialog(message='Select a export method!')

    def exportPartialNamespaces(self):
        ''' export partial sub routine '''
        logger.info('Exporting -> Partial Namespaces')

        # get selected namespaces
        selected_parts = self.getListSelection()

        # check something selected!
        if not selected_parts:
            cmds.confirmDialog(message='Select Something to export')
            return

        # group by namespace!
        namespace_map = collections.defaultdict(list)
        for part in selected_parts:
            namespace = self.getNameSpace(part)
            namespace_map[namespace].append(part)

        start, end = self.getFrameRange()

        strip_ns = self.strip_namespaces

        # cache name space
        for namespace, root_list in namespace_map.items():
            abc_path = self.buildPathWithTemplate(namespace)
#             abc_path = self._buildExportPath(namespace)
            exportAbcCache(start, end, root_list, abc_path, strip_namespaces=strip_ns)

        if self.show_folder:
            self.showFolder(namespace)

    def exportFullNamespaces(self):
        ''' export full sub routine '''
        logger.info('Exporting -> Full Namespaces')

        # get selected namespaces
        selected_ns = self.getSelectedNamespaces()

        # check something selected!
        if not selected_ns:
            cmds.confirmDialog(message='Select Something to export')
            return

        start, end = self.getFrameRange()

        # cache name space
        for namespace in selected_ns:
            self.exportNamespace(namespace, start, end)

        if self.show_folder:
            self.showFolder(namespace)

    def showFolder(self, any_namespace):
        ''' builds a sample path and opens a windows explorer at that folder '''
        path = self.buildPathWithTemplate(any_namespace)
#         path = self._buildExportPath(any_namespace)
        dirname = os.path.dirname(path)
        os.system('explorer.exe "{}"'.format(dirname.replace('/', '\\')))

    def getFrameRange(self):
        ''' gets the frame range form the ui
        Args:
            None
        Returns:
            tuple(int, int): the start and end frame
        '''
        start = self.spinBox_start.value()
        end = self.spinBox_end.value()
        return start, end

    def exportNamespace(self, namespace, start, end):
        ''' exports a whole namespace to alembic
        Args:
            namespace (str): the namespace to export
            start (int): the start frame for the export
            end (int): the end frame for the export
        Returns:
            None
        '''

        logger.info('Exporting namespace {} from {} to {}'.format(namespace, start, end))

        # get all transforms in namespace
        root_list = self._getNamespaceTransforms(namespace)

        # build export path
        abc_path = self.buildPathWithTemplate(namespace)

        strip_ns = self.strip_namespaces
        # export
        exportAbcCache(start, end, root_list, abc_path, strip_namespaces=strip_ns)

    def _getNamespaceTransforms(self, namespace):
        ''' returns all transforms for the namespace
        Args:
            namespace (str): the namespace to look for trnasforms
        Returns:
            list[str]: the list of transforms
        '''

        vis_mesh_list = cmds.ls(v=True, type='mesh')

        mesh_list = [m for m in vis_mesh_list if m.startswith(namespace + ':')]

        transform_list = []
        for m in mesh_list:
            if m.startswith(namespace + ':'):
                transform = cmds.listRelatives(m, p=True, pa=True)[0]
                transform_list.append(transform)

        return transform_list

    def _buildExportPath(self, namespace):
        ''' returns the export path based on the namespace. This is the
        default method in case the template method fails
        Args:
            namespace (str): the namespace to look for trnasforms
        Returns:
            str: the full path to the cache for that namespace
        '''
        this_scene = cmds.file(q=True, sn=True)
        base_folder = os.path.dirname(this_scene)
        cache_folder = os.path.join(base_folder, 'cache')

        # check folder exists
        if not os.path.isdir(cache_folder):
            os.makedirs(cache_folder)

        cache_path = os.path.join(cache_folder, '{}.abc'.format(namespace))

        safe_path = cache_path.replace('\\', '/')
        return safe_path

    # update path sample ---

    @property
    def scene_template(self):
        ''' returns the scene template entered on the ui
        Args:
            None
        Returns:
            lucidity.Template or None: the built template or None if it failed to build
        '''
        return self._buildTemplateHelper(self.scene_template_lineEdit)

    @scene_template.setter
    def scene_template(self, value):
        ''' sets the scene template entered on the ui '''
        self.scene_template_lineEdit.setText(value)

    def setDefaultSceneTemplate(self):
        ''' sets the scene template to a default value '''
        pattern = '{folder}/{scene}.{extension}'
        self.scene_template = pattern

    @property
    def output_template(self):
        ''' returns the output template entered on the ui
        Args:
            None
        Returns:
            lucidity.Template or None: the built template or None if it failed to build
        '''
        return self._buildTemplateHelper(self.out_template_lineEdit)

    @output_template.setter
    def output_template(self, value):
        ''' sets the scene template entered on the ui '''
        self.out_template_lineEdit.setText(value)

    def setDefaultOutputTemplate(self):
        ''' sets the output template to a default value '''
        pattern = '{folder}/caches/{namespace}.abc'
        self.output_template = pattern

    def _buildTemplateHelper(self, line_edit):
        ''' helper to retur templates from the text entered on the ui
        Args:
            line_edit (QtWidgets.QLineEdit): the line edit holding the pattern for the template
        Returns:
            lucidity.Template or None: the built template or None if it failed to build
        '''

        # get the text and check it has something
        text = line_edit.text()
        if not text:
            return None

        # unify the path to lowercase and unix bars to facilitate string matching
        text = self.unifyPath(text)

        # try to build a template with the text. This may fail if pattern is invalid
        try:
            template = self.getLucidityTemplate(text)
        except Exception, e:
            message = 'Could not get template because {}'.format(e)
            logger.warning(message)
            return None

        return template

    def updatePathSample(self):
        ''' Updated the path to export on the ui label '''

        logger.info('updatePathSample')

        # depending on what method is enabled, we get the namespace differently
        if self.export_type == 'full':

            selected_ns = self.getSelectedNamespaces()

            # check there is at least a namespace selected
            if not selected_ns:
                message = 'no item selected on list!'
                logger.warning(message)
                self.setPathSampleDefault()
                return

            namespace = selected_ns[0]

        else:

            selected_parts = self.getListSelection()

            # check there is at least an item selected
            if not selected_parts:
                message = 'no item selected on list!'
                logger.warning(message)
                self.setPathSampleDefault()
                return

            namespace = self.getNameSpace(selected_parts[0])

        # get a path with the templates
        path = self.buildPathWithTemplate(namespace)

        # set it on the ui
        self.setPathSampleDefault(path)

    def setPathSampleDefault(self, path=None):
        ''' sets a text on the path sample lable. If not argument passed,
        it will set a default message
        '''
        if path is None:
            path = 'Select item to update path'
        self.sample_label.setText(path)

    def parseScene(self):
        ''' gets data from scene using lucidity templates
        Args:
            None
        Returns:
            dict: the data extracted from scene path  with scene template
        '''

        scene_data = {}

        # get the scene template and check its ok
        template = self.scene_template

        if template is None:
            message = 'Could not use template system. Using default'
            logger.warning(message)
            return scene_data

        # get maya scene and unify the path
        maya_scene = cmds.file(q=1, sn=1)
        maya_scene = self.unifyPath(maya_scene)

        # try to parse with scene template. This may fail if pattern does not match
        # with scene name
        try:
            scene_data = template.parse(maya_scene)
        except Exception, e:
            message = 'Could not use parse path because {}. Using default'.format(e)
            logger.warning(message)

        return scene_data

    def buildPathWithTemplate(self, namespace):
        ''' builds a path with the data from scene path and the namespace using
        the output template
        Args:
            namespace (str): the namespace to export
        Returns:
            str: the path builded
        Notes:
            if the build process with templates fails, it returns a default path
        '''

        keys = self.parseScene()

        keys['namespace'] = namespace

        # get ouput template and check it's ok
        template = self.output_template
        if template is None:
            message = 'Could not use template system. Using default'
            logger.warning(message)
            return self._buildExportPath(namespace)

        # try to format the values in keys variable with template
        try:
            path = template.format(keys)
        except Exception, e:
            message = 'Could not use template system because {} Using default'.format(e)
            logger.warning(message)
            return self._buildExportPath(namespace)

        return path

    def getLucidityTemplate(self, pattern):
        ''' returns a lucidity template with our custom settings'''

        # modify the default place holder to admit : and /
        default_placeholder_expression = '[\w_.\-:/]+'
        template = lucidity.Template('temp', pattern,
                                     default_placeholder_expression=default_placeholder_expression,
                                     duplicate_placeholder_mode=lucidity.Template.STRICT,
                                     anchor=lucidity.Template.ANCHOR_BOTH)

        return template

    def unifyPath(self, path):
        ''' unify a path to lower case and unix bars. For example:
            C:\Windows -> c:/windows
        Args:
            path (str): the path to unify
        Returns:
            str: the unified path
        '''

        safe_path = os.path.normpath(path)
        safe_path = safe_path.replace('\\', '/')
        safe_path = safe_path.lower()
        return safe_path


def exportAbcCache(start, end, root_list, abc_path, strip_namespaces):
    ''' Exports an abc cache
    Args:
        start (int): start frame for the cache
        end (int): end frame for the cache
        root_list (list[str]): the list of roots to cache
        abc_path (str): full path to the abc file to export
    Returns:
        None
    '''

    logger.info('Exporting {} transforms to {}'.format(len(root_list), abc_path))

    strip_flag = '-stripNamespaces ' if strip_namespaces else ''

    root_list_str = ''.join([' -root {} '.format(r) for r in root_list])
    cmd = 'AbcExport  -j " -fr {start} {end} -writeVisibility {strip_flag} {root_list_str} -uvWrite -file \\"{abc_path}\\" " ;'
    resolved_cmd = cmd.format(**locals())

    # check fodler exists
    dirname = os.path.dirname(abc_path)
    if not os.path.isdir(dirname):
        os.makedirs(dirname)

    logger.info(resolved_cmd)
    loadAlembicPlugins()
    mel.eval(resolved_cmd)


def loadAlembicPlugins():
    ''' load alembic plugins '''
    loadPlugin(pluginName=ABC_EXPORT_PLUGIN)


def loadPlugin(pluginName):
    ''' load a plugin
    Args:
        pluginName (str): the name of the plugin to load
    Returns:
        None
    '''
    if cmds.pluginInfo(pluginName, q=1, loaded=1):
        message = 'Already loaded plugin {}'.format(pluginName)
        logger.warning(message)
        return

    message = 'Loading plugin {}'.format(pluginName)
    logger.info(message)
    cmds.loadPlugin(pluginName)       


def getMayaMainWindowAsQWidget():
    ''' Returns the maya main window as a Pyside.QWidget '''

    # get pointer to main window object in memory
    from maya import OpenMayaUI as omui
    ptr = omui.MQtUtil.mainWindow()

    # wrap the object at that memory location with a QWidget
    from PySide2 import QtWidgets
    from shiboken2 import wrapInstance
    widget = wrapInstance(long(ptr), QtWidgets.QWidget)

    # return the qwidget
    return widget


def main():
    # get parent (maya window)
    parent = getMayaMainWindowAsQWidget()

    # create the ui and show it
    ui = MainUI(parent=parent)
    ui.show()

    # return it so it is not garbage collected
    return ui
